//Digole Digital Solutions: www.digole.com

/* library for the Digole Displays
 *
 * changes by Hartmut Eilers <hartmut@eilers.net>
 * 
 * for my usage only SPI is used - deleted all UART and I2C
 * changed write SPI from bitbanging to hardware SPI
 * added some function for compatibility to the oled library
 * removed everything not needed for my usecase
 * 
 */

#ifndef DigoleSerialDisp_h
#define DigoleSerialDisp_h

#include <inttypes.h>
#include "Print.h"
#include <SPI.h>
#include <Arduino.h>
//#include <avr/wdt.h>
// Communication set up command
// Text function command
// Graph function command
#define Serial_UART 0
#define Serial_I2C 1
#define Serial_SPI 2
#define _TEXT_ 0
#define _GRAPH_ 1
#ifdef FLASH_CHIP   //if writing to flash chip
#define _WRITE_DELAY 40
#else   //if writing to internal flash
#define _WRITE_DELAY 100
#endif
#ifndef Ver
#define Ver 40
#endif

/*
 * Due to errors like clearScreen did not work, setBgColor had crazy colors
 * I debugged a lot, till I noticed that with ESC cmds everything worked like
 * described in the manual. The usual way, by using commands like TT or BGC
 * I measured completly different stuff on the SPI Bus as expected. 
 * After reading the manual regarding ESC cmds and the proposed speed fortune
 * I decided to change all library cmds to their ESC equivalents.
 * 
 * Table from manual:
 * “TT” (1), "ETB" (2), "ETP" (3), "ETO" (4), "ESC" (5), "SI2CA" (6), "SC" (7), 
 * "SB" (8), "SD" (9), "SF" (10),"SSS" (11), "SUF" (12), "SOO" (13), "SLP" (14), 
 * "FR" (15), "LN" (16), "LT" (17), "DC" (18), "DIM" (19), "DP" (20),"DR" (21), 
 * "DSS" (22), "DOUT" (23), "TP" (24), "BL" (25), "TRT" (26), "GP" (27), "CL" (28), 
 * "CC" (29), "CS" (30),“CT" (31), “MA" (32), “MCD" (33), "MDT" (34), "DM" (35), 
 * "EDIM" (36), "FS" (37), "WREP" (38), "RDEP" (39), "INV" (40),"DNALL" (41)
 * , "DNMCU" (42), "SLCD" (43), "RPNXY" (44), "TUCHC" (45), "RDBAT" (46)
 * , "RDAUX" (47), "RDTMP" (48), "FLMER" (49), "FLMRD" (59),"FLMWR" (51), 
 * "FLMCS" (52), "BGC" (53), "DWWIN" (54), "RSTDW" (55), "WINCL" (56), 
 * "SPIMD" (57), "FTOB" (58), “DLY” (59), "TRANS" (60),“VIDEO" (61)
 * 
 */
 
class Digole {
public:

    virtual size_t read1(void);
    virtual void cleanBuffer(void);
//    virtual int readInt(void);
};

class DigoleSerialDisp : public Print, public Digole {
public:

    void begin(void) {
        //pinMode(_Clockpin, OUTPUT);
        //pinMode(_Datapin, OUTPUT);
        pinMode(_SSpin, OUTPUT);
        digitalWrite(_SSpin, HIGH);
        //digitalWrite(_Clockpin, LOW);
        //digitalWrite(_Datapin, LOW);
        //if (_SIpin != 0) {
        //    pinMode(_SIpin, INPUT);
        //}
    }

    DigoleSerialDisp(uint8_t pin_data, uint8_t pin_clock, uint8_t SS, uint8_t SI = 0) //spi set up
    {
        _Clockpin = pin_clock;
        _Datapin = pin_data;
        _SSpin = SS;
        _SIpin = SI;
        begin();
        #ifdef ESP8266
        SPI2->begin();
        #endif
        #ifdef ESP32
        SPI2->begin(pin_clock,SI,pin_data,SS);
        #endif
        SPI2->setFrequency(1000000);
        SPI2->setBitOrder(MSBFIRST);
    }

    size_t write(uint8_t value) {
        digitalWrite(_SSpin, LOW);
        digitalWrite(_SSpin, LOW);
        digitalWrite(_SSpin, LOW);
        SPI2->write(value);
        digitalWrite(_SSpin, HIGH);
        return 1; // assume sucess
    }

    size_t read1(void) {
        int t = 0;
        char c;
//        wdt_disable();
        digitalWrite(_SSpin, HIGH);
        digitalWrite(_SSpin, HIGH);
        while (digitalRead(_SIpin) == LOW) ; //check to see the data is ready(1) or not(0)
        digitalWrite(_SSpin, LOW); //tell display module I will read data
        digitalWrite(_SSpin, LOW); //delay about 5us to wait module prepare data
        digitalWrite(_SSpin, LOW); 
        for (c = 8; c > 0; c = c - 1) {
            t <<= 1;
            digitalWrite(_Clockpin, HIGH);
            if (digitalRead(_SIpin))
                t |= 1;
            digitalWrite(_Clockpin, LOW);
        }

        //  t=shiftIn(_Datapin, _Clockpin, MSBFIRST);
        digitalWrite(_SSpin, HIGH);
        return t; // assume sucess
    }
    void cleanBuffer(void)
    {
    }

    /*---------fucntions for Text and Graphic LCD adapters---------*/
    void writeStr(const char *s) {
        int i = 0;
        while (s[i] != 0)
            write(s[i++]);
    }

    void disableCursor(void) {
        writeStr("CS0");
    }

    void enableCursor(void) {
        writeStr("CS1");
    }

    void clearScreen(void) {
        writeStr("CL");
    }

    void setI2CAddress(uint8_t add) {
        writeStr("SI2CA");
        write(add);
        _I2Caddress = add;
    }

    void displayConfig(uint8_t v) {
        writeStr("DC");
        write(v);
    }
    //print function

    void println(const __FlashStringHelper *v) {
        preprint();
        Print::println(v);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(const String &v) {
        preprint();
        Print::println(v);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(const char v[]) {
        preprint();
        Print::println(v);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(char v) {
        preprint();
        Print::println(v);
        Print::println("\x0dTRT");
    }

    void println(unsigned char v, int base = DEC) {
        preprint();
        Print::println(v, base);
        Print::println("\x0dTRT");
    }

    void println(int v, int base = DEC) {
        preprint();
        Print::println(v, base);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(unsigned int v, int base = DEC) {
        preprint();
        Print::println(v, base);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(long v, int base = DEC) {
        preprint();
        Print::println(v, base);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(unsigned long v, int base = DEC) {
        preprint();
        Print::println(v, base);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(double v, int base = 2) {
        preprint();
        Print::println(v, base);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(const Printable& v) {
        preprint();
        Print::println(v);
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void println(void) {
        write((uint8_t) 0);
        writeStr("TRT");
    }

    void print(const __FlashStringHelper *v) {
        preprint();
        Print::print(v);
        write((uint8_t) 0);
    }

    void print(const String &v) {
        preprint();
        Print::print(v);
        write((uint8_t) 0);
    }

    void print(const char v[]) {
        preprint();
        Print::print(v);
        write((uint8_t) 0);
    }

    void print(char v) {
        preprint();
        Print::print(v);
        write((uint8_t) 0);
    }

    void print(unsigned char v, int base = DEC) {
        preprint();
        Print::print(v, base);
        write((uint8_t) 0);
    }

    void print(int v, int base = DEC) {
        preprint();
        Print::print(v, base);
        write((uint8_t) 0);
    }

    void print(unsigned int v, int base = DEC) {
        preprint();
        Print::print(v, base);
        write((uint8_t) 0);
    }

    void print(long v, int base = DEC) {
        preprint();
        Print::print(v, base);
        write((uint8_t) 0);
    }

    void print(unsigned long v, int base = DEC) {
        preprint();
        Print::print(v, base);
        write((uint8_t) 0);
    }

    void print(double v, int base = 2) {
        preprint();
        Print::print(v, base);
        write((uint8_t) 0);
    }

    void print(const Printable& v) {
        preprint();
        Print::print(v);
        write((uint8_t) 0);
    }
    void preprint(void);
    int readInt(void);
    /*----------Functions for Graphic LCD/OLED adapters only---------*/
    //the functions in this section compatible with u8glib
    void drawBitmap(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const uint8_t *bitmap);
    void drawBitmap256(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const uint8_t *bitmap);
    void drawBitmap65K(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const uint8_t *bitmap);
    void drawBitmap262K(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const uint8_t *bitmap);
    void setTrueColor(uint8_t r, uint8_t g, uint8_t b);
    void setRot90(void);
    void setRot180(void);
    void setRot270(void);
    void undoRotation(void);
    void setRotation(uint8_t);
    void setContrast(uint8_t);
    void drawBox(unsigned int x, unsigned int y, unsigned int w, unsigned int h);
    void drawCircle(unsigned int x, unsigned int y, unsigned int r, uint8_t = 0);
    void drawDisc(unsigned int x, unsigned int y, unsigned int r);
    void drawFrame(unsigned int x, unsigned int y, unsigned int w, unsigned int h);
    void drawPixel(unsigned int x, unsigned int y, uint8_t color);
    void drawPixel(unsigned int x, unsigned int y);
    void drawLine(unsigned int x, unsigned int y, unsigned int x1, unsigned int y1);
    void drawLineTo(unsigned int x, unsigned int y);
    void drawHLine(unsigned int x, unsigned int y, unsigned int w);
    void drawVLine(unsigned int x, unsigned int y, unsigned int h);
    //-------------------------------
    //special functions for our adapters
    //void uploadStartScreen(int lon, const unsigned char *data); //upload start screen
    void setFont(uint8_t font); //set font, availale: 6,10,18,51,120,123, user font: 200-203
    void nextTextLine(void); //got to next text line, depending on the font size
    void setColor(uint8_t); //set color for graphic function
    void backLightOn(void); //Turn on back light
    void backLightOff(void); //Turn off back light
    void screenOnOff(uint8_t); //turn screen on/off
    void cpuOff(void); //put MCU in sleep, it will wake up when new data received
    void moduleOff(void); //put whole module in sleep: Back light off, screen in sleep, MCU in sleep
    void backLightBrightness(uint8_t); //set backlight brightness,0~100

    void directCommand(uint8_t d); //send command to LCD drectly
    void directData(uint8_t d); //send data to LCD drectly
    void moveArea(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h, char xoffset, char yoffset); //move a area of screen to another place
    void drawStr(unsigned int x, unsigned int y, const char *s);
    void setPrintPos(unsigned int x, unsigned int y, uint8_t = 0);
    void setLCDColRow(uint8_t col, uint8_t row);
    void setTextPosAbs(unsigned int x, unsigned int y);
    /*-----Touch screen functions---*/
    void calibrateTouchScreen(void);
    void readTouchScreen(void);
    void readTouchScreen(int *x,int *y);
    void readClick(void);
    void readClick(int *x,int *y);
    int readBattery(void);
    int readAux(void);
    int readTemperature(void);

    /*-----Flash memory functions---*/
    void flashErase(unsigned long int addr, unsigned long int length);
    void flashReadStart(unsigned long int addr, unsigned long int len);
    void setFlashFont(unsigned long int addr);
    void runCommandSet(unsigned long int addr);
    size_t read(void);

    void write2B(unsigned int v);
    //--- new function on V3.3 firmware ----//
    void writeE2prom(unsigned int addr, unsigned int len, unsigned char *data);
    void readE2prom(unsigned int addr, unsigned int len);

    //--- new function on V3.0 firmware ----//

    void setBgColor(uint8_t color) //set current color as background
    {
        // as of ver 4 of Display Firmware one can use ESC cmds
        write(27);
        write(53);
        write(color);
    }

    void setDrawWindow(unsigned int x, unsigned int y, unsigned int width, unsigned int height) {
        writeStr("DWWIN");
        write2B(x);
        write2B(y);
        write2B(width);
        write2B(height);
    }

    void resetDrawWindow(void) {
        writeStr("RSTDW");
    }

    void cleanDrawWindow(void) {
        writeStr("WINCL");
    }
    //---end of V3.0 functions

    void displayStartScreen(uint8_t m) {
        writeStr("DSS");
        write(m);
    } //display start screen

    void setMode(uint8_t m) {
        writeStr("DM");
        write(m);
    } //set display mode

    void setTextPosBack(void) {
        writeStr("ETB");
    } //set text position back to previous, only one back allowed

    void setTextPosOffset(char xoffset, char yoffset) {
        writeStr("ETO");
        write(xoffset);
        write(yoffset);
    }

    void setLinePattern(uint8_t pattern) {
        writeStr("SLP");
        write(pattern);
    }

    void setLCDChip(uint8_t chip) { //only for universal LCD adapter
        writeStr("SLCD");
        write(chip);
    }

    void setBackLight(uint8_t bl) {
        writeStr("BL");
        write(bl);
    }

    void digitalOutput(uint8_t x) {
        writeStr("DOUT");
        write(x);
    }

    void flushScreen(uint8_t bl) {
        writeStr("FS");
        write(bl);
    }

    void downloadStartScreen(int lon, const unsigned char *data) {
        int j;
        unsigned char b;
        uint8_t c;
        writeStr("SSS");
        lon++;
        write((uint8_t) (lon / 256));
        write((uint8_t) (lon % 256));
        delay(300);
        b = 0;
        for (j = 0; j < (lon - 1); j++) {
            c = pgm_read_byte_near(data + j);
            write(c);
            if ((++b) == 64) {
                b = 0, delay(_WRITE_DELAY);
            }
        }
        write(255); //indicater of end of it
        delay(_WRITE_DELAY);
    }

    void downloadUserFont(int lon, const unsigned char *data, uint8_t sect) {
        uint8_t c;
        unsigned char b;
        writeStr("SUF");
        write(sect);
        delay(300);
        write((uint8_t) (lon % 256));
        write((uint8_t) (lon / 256));
        b = 0;
        for (int j = 0; j < lon; j++) {
            c = pgm_read_byte_near(data + j);
            write(c);
            if ((++b) == 64) {
                b = 0, delay(_WRITE_DELAY);
            }
        }
    }

    void flashWrite(unsigned long int addr, unsigned long int len, const unsigned char *data) {
        unsigned char c, b;
        unsigned long int i;
        writeStr("FLMWR");
        write(addr >> 16);
        write(addr >> 8);
        write(addr);
        write(len >> 16);
        write(len >> 8);
        write(len);
        b = 0;
        for (i = 0; i < len; i++) {
            c = pgm_read_byte(data + i);
            write(c);
            if ((++b) == 64) {
                b = 0, delay(_WRITE_DELAY);
            }
        }
        delay(_WRITE_DELAY);
#ifdef FLASH_CHIP
        //check write memory done
        while (read1() != 17);
#endif
    }

    void flashWrite(unsigned long int addr, unsigned long int len, unsigned char *data) {
        unsigned char c, b;
        unsigned long int i;
        writeStr("FLMWR");
        write(addr >> 16);
        write(addr >> 8);
        write(addr);
        write(len >> 16);
        write(len >> 8);
        write(len);
        b = 0;
        for (i = 0; i < len; i++) {
            c = data[i];
            write(c);
            if ((++b) == 64) {
                b = 0, delay(_WRITE_DELAY);
            }
        }
#ifdef FLASH_CHIP
        //check write memory done
        while (read1() != 17);
#endif
    }
    void manualCommand(unsigned char c)
    {
        writeStr("MCD");
        write(c);
    }
    void manualData(unsigned char d)
    {
        writeStr("MDT");
        write(d);
    }
    void setSPIMode(unsigned char mode) {
        if (mode < 4) {
            writeStr("SPIMD");
            write(mode);
        }
    }

    /* 
     * compatibility layer for the OLED display's with either SDD1306 or with SH1106 controller
     * see: https://gitlab.com/lnetwalker/oled/-/tree/master
     */

    void OledDisp(int offset,int OLEDheight);
    void sendcommand(unsigned char com);
    void displayOn(void);
    void reset_display(void);
    void displayOff(void);
    void clear_display(void);
    void sendChar(unsigned char data);
    void sendCharXY(unsigned char data, int X, int Y);
    void sendBigCharXY(unsigned char data, int X, int Y);
    void sendBigStrXY( const char *string, int X, int Y);
    void setXY(unsigned char row,unsigned char col);
    void sendStr(unsigned char *string);
    void sendStrXY( char *string, int X, int Y);
    void init_OLED(void);
    void deleteBuffer(void);
    void displayBuffer(void);
    void displayLine(int X1,int Y1,int X2,int Y2);
    void setPixel(int X,int Y);
    void drawCircle(int xc, int yc, int r);
    void circleBres(int xc, int yc, int x, int y);
    void heartbeat(int X,int Y);

private:
    unsigned long _Baud;
    HardwareSerial *_mySerial;
    uint8_t _I2Caddress;
    uint8_t _Clockpin;
    uint8_t _Datapin;
    uint8_t _SSpin;
    uint8_t _SIpin;
    uint8_t _Comdelay;
    #ifdef ESP32
    SPIClass * SPI2 = new SPIClass(HSPI);  // pointer to SPI Class
    #endif
    #ifdef ESP8266
    SPIClass * SPI2 = new SPIClass();  // pointer to SPI Class
    #endif
};

#endif
